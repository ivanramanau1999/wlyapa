﻿using BichkovWapoval.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BichkovWapoval.Data
{
    public class WapovalContext : DbContext
    {
        public DbSet<Tour> Tours { get; set; }
        public DbSet<Contract> Contracts { get; set; }

        public WapovalContext(DbContextOptions<WapovalContext> options)
            : base(options)
        {
            Database.EnsureCreated();   // создаем базу данных при первом обращении
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // configures one-to-many relationship
            modelBuilder.Entity<Contract>()
                .HasOne(c => c.Tour);

        }

        public DbSet<BichkovWapoval.Models.EmployeeWithContractsCount> EmployeeWithContractsCount { get; set; }

        public DbSet<BichkovWapoval.Models.TourWithPrice> TourWithPrice { get; set; }
    }
}
