﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BichkovWapoval.Models
{
    public enum ContractState
    {
        IdAsc,
        IdDesc,
        EmployeeNameAsc,
        EmployeeNameDesc
    }
}
