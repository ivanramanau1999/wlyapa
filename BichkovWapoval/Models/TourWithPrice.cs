﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BichkovWapoval.Models
{
    public class TourWithPrice
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CountOfDays { get; set; }
        public long Price { get; set; }
    }
}
