﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BichkovWapoval.Models
{
    public class EmployeeWithContractsCount
    {
        public int Id { get; set; }
        public string EmployeeName { get; set; }
        public int CountOfContracts { get; set; }
    }
}
