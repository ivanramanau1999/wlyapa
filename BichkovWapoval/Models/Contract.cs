﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BichkovWapoval.Models
{
    public class Contract
    {
        public int Id { get; set; }
        public int ContractNumber { get; set; }
        public virtual Tour Tour { get; set; }
        public DateTime ContractDate { get; set; }
        public string EmployeeName { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public long Price { get; set; }
        public int CountOfPlaces { get; set; }
    }
}
