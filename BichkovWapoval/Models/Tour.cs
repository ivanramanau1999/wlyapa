﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BichkovWapoval.Models
{
    public class Tour
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CountOfDays {get;set;}
        public string Destination { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public int HotelStarts { get; set; }
    }
}
