﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BichkovWapoval.Data;
using BichkovWapoval.Models;

namespace BichkovWapoval.Controllers
{
    public class ContractsController : Controller
    {
        private readonly WapovalContext _context;

        public ContractsController(WapovalContext context)
        {
            _context = context;
        }

        // GET: Contracts
        public async Task<IActionResult> Index(ContractState sortOrder = ContractState.IdAsc)
        {
            IQueryable<Contract> contracts = _context.Contracts;
            ViewData["IdSort"] = sortOrder == ContractState.IdAsc ? 
                ContractState.IdDesc : ContractState.IdAsc;
            ViewData["EmployeeNameSort"] = sortOrder == ContractState.EmployeeNameAsc ? 
                ContractState.EmployeeNameDesc : ContractState.EmployeeNameAsc;

            contracts = sortOrder switch
            {
                ContractState.IdDesc => contracts.OrderByDescending(s => s.Id),
                ContractState.EmployeeNameAsc => contracts.OrderBy(s => s.EmployeeName),
                ContractState.EmployeeNameDesc => contracts.OrderByDescending(s => s.EmployeeName),
                _ => contracts.OrderBy(s => s.Id),
            };
            return View(await contracts.AsNoTracking().ToListAsync());
        }

        // GET: Contracts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contract = await _context.Contracts
                .FirstOrDefaultAsync(m => m.Id == id);
            if (contract == null)
            {
                return NotFound();
            }

            return View(contract);
        }

        // GET: Contracts/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Contracts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ContractNumber,ContractDate,EmployeeName,CustomerName,CustomerPhone,Price,CountOfPlaces")] Contract contract)
        {
            if (ModelState.IsValid)
            {
                _context.Add(contract);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(contract);
        }

        // GET: Contracts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contract = await _context.Contracts.FindAsync(id);
            if (contract == null)
            {
                return NotFound();
            }
            return View(contract);
        }

        // POST: Contracts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ContractNumber,ContractDate,EmployeeName,CustomerName,CustomerPhone,Price,CountOfPlaces")] Contract contract)
        {
            if (id != contract.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(contract);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ContractExists(contract.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(contract);
        }

        // GET: Contracts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contract = await _context.Contracts
                .FirstOrDefaultAsync(m => m.Id == id);
            if (contract == null)
            {
                return NotFound();
            }

            return View(contract);
        }

        // POST: Contracts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var contract = await _context.Contracts.FindAsync(id);
            _context.Contracts.Remove(contract);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ContractExists(int id)
        {
            return _context.Contracts.Any(e => e.Id == id);
        }

        [HttpGet]
        [Route("/date/{date}")]
        public async Task<IActionResult> GetContractByDate(DateTime date)
        {
            var d = date.Date;
            var contracts = await _context.Contracts.Where(c => c.ContractDate.Date.CompareTo(date.Date) == 0)
                .ToListAsync();
            return View(contracts);
        }


        [HttpGet]
        public async Task<IActionResult> GetEmployeesWithMostContracts()
        {
            var employees = await _context.Contracts
                .GroupBy(c => c.EmployeeName)
                .Select(e => new EmployeeWithContractsCount
                {
                    Id = 1,
                    EmployeeName = e.Key, 
                    CountOfContracts = e.Count() 
                }).ToListAsync();
            

            return View(employees);
        }

        
    }
}
